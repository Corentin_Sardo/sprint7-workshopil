public class Book
{
    private String titre;
    private String auteur;
    private String esbn;

    Book(String titre, String auteur, String esbn){
        this.titre = titre;
        this.auteur = auteur;
        this.esbn = esbn;
    }
}
